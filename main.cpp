#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "ClockList.h"

using namespace std;

/* 
 * Χρονικό όριο στο οποίο για να είναι μια σελίδα στο
 * σύνολο εργασίας θα πρέπει να μην ξεπερνά.
 * 
 * Η τιμή αυτή μπορεί να αλλάξει κατά την μεταγλώττιση,
 * προεπιλεγμένα είναι 500 διότι παρατήρησα ότι με αυτήν
 * την τιμή έχω τα λιγότερα σφάλματα σελίδας κατά μέσο όρο.
 */
#define t 500

int main()
{
    srand(time(NULL));

    CList ws_clock;
    Page page_table[128];

    // Θέτω το ID κάθε σελίδας με έναν αύξοντα αριθμό.
    for (int i=0; i < 128; i++)
        page_table[i].ID = i;

    // Εδώ κρατάω τις 20 σελίδες που έγινε αναφορά.
    int ref_table[20];

    int random_ref;
    int page_faults = 0;
    int used_frames = 0;

    for (int i=0; i < 500; i++)
    {
        int j;

        for (j=0; j < 20; j++)
        {
            random_ref = rand() % 127;

            // Δεν μπορεί να γίνει αναφορά σε σελίδα παραπάνω από μια φορά.
            if (page_table[random_ref].A)
            {
                j--;
                continue;
            }

            page_table[random_ref].A = true;
            ref_table[j] = random_ref;
            
            if (rand() % 100 + 1 <= 25)
                page_table[random_ref].T = true;

            /* 
             * Προσθέτω το i*20 και το j για να υπολογίσω τον χρόνο τελευταίας αναφοράς.
             * Η πρώτη αναφορά θα γίνει σε χρόνο 0 η δεύτερη 1, ..., η τελευταία 9999.
             */
            page_table[random_ref].last_ref = i*20 + j;
        }


        /*
         * Έγιναν οι 20 αναφορές, τώρα από τις 20 αυτές σελίδες
         * θα δούμε ποιες θα φύγουν από την μνήμη, ποιες θα
         * μπούνε στην μνήμη και ποιες θα παραμείνουν.
         * 
         * Θα σαρώσουμε των πίνακα αναφορών, ο οποίος περιέχει
         * τις 20 αναφορές.
         */
        for (j=0; j < 20; j++)
        {
            // Βρίσκεται σε πλαίσιο.
            if (page_table[ref_table[j]].P)
            {
                // Η σελίδα θα φύγει.
                if (rand() % 100 + 1 <= 40)
                    ws_clock.RemovePage(&page_table[ref_table[j]]);

                // Δεν χρειάζεται να γίνει κάτι για την σελίδα που θα παραμείνει.
            }
            // ΔΕΝ βρίσκεται σε πλαίσιο.
            else
            {
                page_faults++;

                // Υπάρχει κενό πλαίσιο.
                if (ws_clock.getSize() < 32)
                {
                    ws_clock.Insert(&page_table[ref_table[j]]);
                }
                // ΔΕΝ υπάρχει κενό πλαίσιο.
                else
                {
                    Page *head = ws_clock.getHead();
                    Page *old_head = ws_clock.getHead();
                    Page *oldest_page = NULL;
                    bool oldest_change = false;

                    /*
                     * Ο αλγόριθμος αλλάζει και ελέγχει τις τιμές tmpA των σελίδων.
                     * Διότι μετά την εισαγωγή τις σελίδας θέλω οι τιμές αναφοράς κάθε
                     * σελίδας να παραμείνει όπως ήταν πριν την εισαγωγή γιατί ο
                     * αλγόριθμος μπορεί να εκτελεστεί ξανά και για άλλη σελίδα
                     * για αυτήν την διακοπή.
                     */
                    ws_clock.ResetTmpA();

                    do
                    {
                        // Αν η σελίδα έχει αναφερθεί.
                        if (head->tmpA)
                        {
                            head->tmpA = false;
                        }
                        // Αν δεν έχει αναφερθεί.
                        else
                        {
                            // Αν η ηλικία της σελίδας έχει ξεπεράσει το όριο t και η σελίδα είναι καθαρή.
                            if (i*20+j - head->last_ref > t && !head->T)
                            {
                                // Η σελίδα αυτή βγαίνει από το σύνολο εργασία.
                                ws_clock.Insert(&page_table[ref_table[j]]);
                                ws_clock.RemoveHead();
                                head = ws_clock.Next();
                                break;
                            }
                            // Αν η ηλικία της σελίδας έχει ξεπεράσει το όριο t αλλά η σελίδα είναι βρώμικη.
                            else if (i*20+j - head->last_ref > t && head->T)
                            {
                                // Στην πραγματικότητα εδώ προγραμματίζετε η εγγραφή της σελίδας στον δίσκο.
                                head->T = false;
                            }
                            // Αν η ηλικία της σελίδας είναι μέσα στο όριο t.
                            else
                            {
                                // Αποθηκεύω την σελίδα ως την πιο παλιά, αν δεν υπήρχε άλλη πιο παλιά.
                                if (oldest_page == NULL)
                                {
                                    oldest_page = head;
                                    oldest_change = true;
                                }
                                else
                                {
                                    // Κρατάω την πιο παλιά σελίδα.
                                    if (head->last_ref < oldest_page->last_ref)
                                    {
                                        oldest_page = head;
                                        oldest_change = true;
                                    }
                                    else
                                    {
                                        oldest_change = false;
                                    }
                                }
                            }
                        }
                        head = ws_clock.Next();

                    } while(oldest_change || head != old_head);

                    // Αν όλες οι σελίδες ήταν στο όριο t και έγινε μια περιστροφή στο ws_clock.
                    if (head == old_head)
                    {
                        ws_clock.Insert(&page_table[ref_table[j]]);
                        ws_clock.RemovePage(oldest_page);
                        break;
                    }                    
                }
            }
            
        }

        // Επαναφορά όλον των αναφορών σε 0 για να γίνει το επόμενο κύμα των 20 αναφορών.
        for (j=0; j < 20; j++)
        {
            page_table[ref_table[j]].A = false;
            page_table[ref_table[j]].T = false;
        }

    }

    cout << "Σφάλματα σελίδας: " << page_faults << endl;
    
    return 0;
}