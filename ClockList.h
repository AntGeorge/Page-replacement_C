#include <iostream>

class Page
{
    public:

        /*
         * load_time: Χρόνος φόρτωσης.
         * last_ref: Χρόνος τελευταίας αναφοράς.
         */
        int load_time, last_ref;

        /*
         * A: Αναφορά.
         * Τ: Τροποποίηση.
         * P: Σελιδοποιημένη, η σελίδα βρίσκετε σε πλαίσιο.
         * ID: Αναγνωριστικό σελίδας.
         */
        bool A, T, P, tmpA;
        int ID;
        Page *next;
    
        Page()
        {
            A = false;
            T = false;
            P = false;
        }
};

class CList
{
    private:
        Page *index;
        int length;

    public:
        CList()
        {
            index = NULL;
            length = 0;
        }

        Page * getHead()
        {
            return index;
        }

        Page * Next()
        {
            index = index->next;
            return index;
        }

        void ResetTmpA()
        {
            Page *t = index;
            
            if (t == NULL) return;
            
            do
            {
                t->tmpA = t->A;
                t = t->next;
            } while(t != index);
        }

        void Insert(Page *n)
        {
            if (index == NULL)
            {
                index = n;
                n->next = index;
            }
            else
            {
                n->next = index->next;
                index->next = n;
            }
            n->P = true;
            length++;
        }

        int getSize()
        {
            return length;
        }

        void RemoveHead()
        {
            if (length==0) return;

            Page *tmp = index;
            index->P = false;

            while(tmp->next != index)
            {
                tmp = tmp->next;
            }

            tmp->next = index->next;
            if (index->next == index)
            {
                index = NULL;
            }
            else
            {
                index = tmp;
            }

            length--;
        }

        void RemovePage(Page *n)
        {
            if (length == 0) return;

            Page *tmp = index;

            while (tmp->next != n)
            {
                tmp = tmp->next;
            }

            if (tmp->next == index)
            {
                if (tmp->next == index->next)
                {
                    index = NULL;
                }
                else
                {
                    index = index->next;
                }
            }
            
            tmp->next = n->next;
            n->P = false;

            length--;
        }

        void PrintList()
        {
            Page * tmp = index;
            std::cout << "Head: " << index->ID << std::endl;
            while (tmp != NULL)
            {
                std::cout << "ID: " << tmp->ID << "\tA: " << tmp->A << "\ttmpA:  " << tmp->tmpA << "\tlast_ref: " << tmp->last_ref << std::endl;
                tmp = tmp->next;
                if (tmp == index) break;
            }
            std::cout << std::endl;
        }
};