#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "ClockList.h"

using namespace std;

int main()
{
    CList ws_clock;
    Page page_table[128];

    for (int i = 0; i < 128; i++)
        page_table[i].ID = i;

    ws_clock.Insert(&page_table[12]);
    ws_clock.Insert(&page_table[11]);
    ws_clock.Insert(&page_table[14]);
    ws_clock.Insert(&page_table[13]);

    ws_clock.PrintList();

    ws_clock.RemovePage(&page_table[12]);
    ws_clock.RemovePage(&page_table[11]);
    ws_clock.RemovePage(&page_table[14]);
    ws_clock.RemovePage(&page_table[13]);

    ws_clock.PrintList();

    return 0;
}